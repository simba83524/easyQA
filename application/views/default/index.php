<?php require_once VIEWPATH . "$theme_id/inc/header.inc.php";?>
<?php if (!empty($user['email']) && $user['email_status'] == 1): ?>
    <div style="max-width: 1210px; margin: 0 auto 10px; padding: 0 5px; color: #f00;">
        <p>
            <span>您绑定的邮箱<?=$user['email']?>还未激活，请进入您的邮箱激活，如果没有收到邮件，请</span>
            <a href="javascript:;" onclick="resend_bind_email();">重发验证邮件</a>
        </p>
    </div>
<?php endif;?>
<div class="main layui-clear">
    <?php require_once VIEWPATH . "$theme_id/inc/topic_nav.inc.php";?>
    <div class="wrap">
        <div class="content">
            <?php if (is_array($article_lists)): ?>
                <ul class="fly-list">
                    <?php foreach ($article_lists as $_article): ?>
                        <li class="fly-list-li">
                            <a href="/u/home/<?=$_article['user_id']?>" class="fly-list-avatar">
                                <img src="<?=create_avatar_url($_article['user_id'], $_article['avatar_ext'])?>" alt="">
                            </a>
                            <h2 class="fly-tip">
                                <a href="/<?=$config['enum_show']['article_type'][$_article['article_type']]?>/detail/<?=$_article['id']?>">
                                    <span class="fly-tip-attile_type_<?=$_article['article_type']?>"><?=$config['enum_show']['article_type_text'][$_article['article_type']]?></span>
                                    <?=$_article['article_title']?>
                                </a>
                                <?php if ($_article['is_top'] == 2): ?>
                                    <span class="fly-tip-stick">置顶</span>
                                <?php endif;?>
                                <?php if ($_article['is_fine'] == 2): ?>
                                    <span class="fly-tip-jing">精帖</span>
                                <?php endif;?>
                            </h2>
                            <p>
                                <span><a href="/u/home/<?=$_article['user_id']?>"><?=$_article['nickname']?></a></span>
                                <span><?=time_tran($_article['add_time'])?></span>
                                <span class="fly-list-hint">
                                    <i class="iconfont" title="回答">&#xe60c;</i> <?=$_article['comment_counts']?>
                                    <i class="iconfont" title="人气">&#xe60b;</i> <?=$_article['view_counts']?>
                                </span>
                            </p>
                        </li>
                    <?php endforeach;?>
                </ul>
            <?php else: ?>
                <div class="fly-none">并无相关数据</div>
            <?php endif;?>

            <?=$page_html?>
        </div>
    </div>
    <div class="edge">
        <?php require_once VIEWPATH . "$theme_id/inc/comment_top_user_lists.inc.php";?>
        <?php require_once VIEWPATH . "$theme_id/inc/codercalendar.inc.php";?>
        <?php require_once VIEWPATH . "$theme_id/inc/q_by_view_hot_lists.inc.php";?>
        <?php require_once VIEWPATH . "$theme_id/inc/q_by_comment_hot_lists.inc.php";?>
        <?php require_once VIEWPATH . "$theme_id/inc/friends_link.inc.php";?>
    </div>
</div>
<?php require_once VIEWPATH . "$theme_id/inc/footer.inc.php";?>