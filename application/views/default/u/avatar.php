<?php require_once VIEWPATH . "$theme_id/inc/header.inc.php";?>
<div class="main layui-clear">
    <div class="wrap">
        <div class="content">
            <?php require_once VIEWPATH . "$theme_id/u/inc/nav.inc.php";?>
            <div class="user-mine">
                <div class="fl mr10" style="width: 75px;">
                    <img id="current_avatar" src="<?=create_avatar_url($user['id'], $user['avatar_ext'])?>" style="display: block; width: 75px; height: 75px;">
                    <div class="tc">
                        <button id="select_avatar_btn" class="layui-btn layui-btn-small layui-btn-normal" style="width: 100%; margin-top: 5px; display: block;">选择图片</button>
                    </div>
                </div>
                <div class="fl" style="max-width: 760px;">
                    <ul class="avatars clearfix">
                        <?php for ($i = 0; $i <= 23; $i++): ?>
                        <li><a href="javascript:;" avatar_name="<?=$i?>.png" onclick="set_system_avatar(this);"><img class="avatar" src="http://<?=$config['qiniu']['static_bucket_domain']?>/avatar/s/<?=$i?>.png!avatar"></a></li>
                        <?php endfor;?>
                    </ul>
                    <ul class="avatars clearfix">
                        <?php for ($i = 100; $i <= 121; $i++): ?>
                        <li><a href="javascript:;" avatar_name="<?=$i?>.png" onclick="set_system_avatar(this);"><img class="avatar" src="http://<?=$config['qiniu']['static_bucket_domain']?>/avatar/s/<?=$i?>.png!avatar"></a></li>
                        <?php endfor;?>
                    </ul>
                    <ul class="avatars clearfix">
                        <?php for ($i = 200; $i <= 223; $i++): ?>
                        <li><a href="javascript:;" avatar_name="<?=$i?>.png" onclick="set_system_avatar(this);"><img class="avatar" src="http://<?=$config['qiniu']['static_bucket_domain']?>/avatar/s/<?=$i?>.png!avatar"></a></li>
                        <?php endfor;?>
                    </ul>
                </div>
            </div>
            <div id="LAY-page"></div>
        </div>
    </div>
    <?php require_once VIEWPATH . "$theme_id/u/inc/sidebar.inc.php";?>
</div>
<script type="text/javascript">
// domain为七牛空间对应的域名，选择某个空间后，可通过 空间设置->基本设置->域名设置 查看获取
// uploader为一个plupload对象，继承了所有plupload的方法
var uploader = Qiniu.uploader({
    runtimes: 'html5,flash,html4',    // 上传模式，依次退化
    browse_button: 'select_avatar_btn',    // 上传选择的点选按钮，必需
    // 在初始化时，uptoken，uptoken_url，uptoken_func三个参数中必须有一个被设置
    // 切如果提供了多个，其优先级为uptoken > uptoken_url > uptoken_func
    // 其中uptoken是直接提供上传凭证，uptoken_url是提供了获取上传凭证的地址，如果需要定制获取uptoken的过程则可以设置uptoken_func
    // uptoken : '<Your upload token>', // uptoken是上传凭证，由其他程序生成
    uptoken_url: '/api/qiniu/uptoken?bucket=' + CONFIG['qiniu']['static_bucket_name'],     // Ajax请求uptoken的Url，强烈建议设置（服务端提供）
    //uptoken_func: function(file){  // 在需要获取uptoken时，该方法会被调用
        // do something
        //alert();
        //return uptoken;
    //},
    get_new_uptoken: true,      // 设置上传文件的时候是否每次都重新获取新的uptoken
    //downtoken_url: '/api/qiniu/private_download_url',
    // Ajax请求downToken的Url，私有空间时使用，JS-SDK将向该地址POST文件的key和domain，服务端返回的JSON必须包含url字段，url值为该文件的下载地址
    // unique_names: true,        // 默认false，key为文件名。若开启该选项，JS-SDK会为每个文件自动生成key（文件名）
    // save_key: true,          // 默认false。若在服务端生成uptoken的上传策略中指定了sava_key，则开启，SDK在前端将不对key进行任何处理
    domain: 'http://' + CONFIG['qiniu']['static_bucket_domain'] + '/',   // bucket域名，下载资源时用到，必需
    //container: 'upload_container',      // 上传区域DOM ID，默认是browser_button的父元素
    max_file_size: '1mb',      // 最大文件体积限制
    flash_swf_url: '/static/lib/plupload/Moxie.swf',  //引入flash，相对路径
    max_retries: 3,          // 上传失败最大重试次数
    dragdrop: false,          // 开启可拖曳上传
    //drop_element: 'upload_container',      // 拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
    chunk_size: '4mb',          // 分块上传时，每块的体积
    auto_start: true,          // 选择文件后自动上传，若关闭需要自己绑定事件触发上传
    filters: {
        mime_types : [ //只允许上传图片和zip文件
            { title : 'Image files', extensions : 'jpg,jpeg,png,gif' }
        ],
        max_file_size : '1024kb', //最大只能上传400kb的文件
        prevent_duplicates : true //不允许选取重复文件
    },
    multi_selection: false,
    init: {
        'FilesAdded': function(up, files) {
            //需要覆盖文件,所以每次需要根据文件名生成可以覆盖上传的uptoken
            var ext = get_ext_with_type(files[0].type);
            var key = 'avatar/' + CONFIG['user']['id'] + '.' + ext;
            Qiniu.uptoken_url = '/api/qiniu/uptoken?bucket=' + CONFIG['static_bucket_name'] + '&type=avatar&key=' + key;
        },
        'BeforeUpload': function(up, file) {
            // 每个文件上传前，处理相关的事情
            layer.load();
        },
        'UploadProgress': function(up, file) {
            // 每个文件上传时，处理相关的事情
        },
        'FileUploaded': function(up, file, info) {
            // 每个文件上传成功后，处理相关的事情
            // 其中info是文件上传成功后，服务端返回的json，形式如：
            // {
            //  "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
            //  "key": "gogopher.jpg"
            //  }
            // 查看简单反馈
            var domain = up.getOption('domain');
            var res = $.parseJSON(info);
            //公有空间链接
            res.url = domain + res.key; //获取上传成功后的文件的Url,如果是私有空间的则不需要此拼装,info里已经带有url
        },
        'Error': function(up, err, errTip) {
            //上传出错时，处理相关的事情
            //文件类型不对
            if(err.code == plupload.FILE_EXTENSION_ERROR){
                layer.msg('文件类型错误。');
            }
            //文件大小超过限制
            else if(err.code == plupload.FILE_SIZE_ERROR){
                layer.msg('文件尺寸超过限制。');
            }
        },
        'UploadComplete': function(up, files, info) {
            //队列文件处理完毕后，处理相关的事情
            //记录数据库中头像图上扩展名
            var ext = get_ext_with_type(files[0].type);
            $.post(
                '/api/account/update_avatar',
                {
                    avatar_ext: ext
                },
                function(json){
                    if(json.error_code == 'ok'){
                        //添加新上传文件显示
                        $('#current_avatar').attr('src', json.avatar);
                        layer.closeAll('loading');
                        layer.msg('头像上传成功。');
                        setTimeout(function(){
                            document.location = document.location;
                        }, 1500);
                    }
                    else{
                        show_error(json.error_code);
                    }
                },
                'json'
            );
        },
        'Key': function(up, file) {
            // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
            // 该配置必须要在unique_names: false，save_key: false时才生效
            var ext = get_ext_with_type(file.type);
            return 'avatar/' + CONFIG['user']['id'] + '.' + ext;
        }
    }
});

//设置系统头像
function set_system_avatar(This){
    var $this = $(This);
    var avatar_name = $this.attr('avatar_name');

    layer.load();
    $.post(
        '/api/account/update_avatar',
        {
            avatar_ext: avatar_name
        },
        function(json){
            if(json.error_code == 'ok'){
                //添加新上传文件显示
                $('#current_avatar').attr('src', json.avatar);
                layer.closeAll('loading');
                layer.msg('头像选择成功。');
                setTimeout(function(){
                    document.location = document.location;
                }, 1500);
            }
            else{
                show_error(json.error_code);
            }
        },
        'json'
    );
}
</script>
<?php require_once VIEWPATH . "$theme_id/inc/footer.inc.php";?>