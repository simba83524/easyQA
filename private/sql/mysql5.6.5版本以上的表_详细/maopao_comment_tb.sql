CREATE TABLE `easyqa`.`maopao_comment_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `maopao_id` INT UNSIGNED NOT NULL COMMENT '提问id',
  `comment_content` TEXT NOT NULL COMMENT '评论内容',
  `reply_comment_id` INT UNSIGNED NULL COMMENT '被回复的评论id,null=非回复评论',
  `dialog_id` BIGINT UNSIGNED NULL COMMENT '对话id,当发生回复评论的时候生成对话id,方便查看对话,null=非对话',
  `vote_counts` INT NOT NULL DEFAULT 0 COMMENT '投票数,可以为负',
  `vote_up_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,顶数',
  `vote_down_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,踩数',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论添加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `maopaoId_index` (`maopao_id`),
  INDEX `replyCommentId_index` (`reply_comment_id`),
  INDEX `dialogId_index` (`dialog_id`),
  INDEX `voteCounts_index` (`vote_counts`),
  INDEX `userId_index` (`user_id`),
  INDEX `addTime_index` (`add_time`)
) ENGINE=INNODB COMMENT='冒泡评论表' CHARSET=utf8 COLLATE=utf8_general_ci;